

var AllTeams = require('./teams.Ctrl');
module.exports = function(app){
    app.get('/teams/home',AllTeams.home);
    app.post('/teams/findapi',AllTeams.findapi);
    app.post('/teams/postapi', AllTeams.postapi)
    app.post('/teams/deleteapi', AllTeams.deleteapi)
 
    app.post('/teams/twotableApi', AllTeams.twotableApi)
    

    app.get('/teams/tCollection',AllTeams.teams);
 
    app.get('/teams/eCollection',AllTeams.employee);
 
    
}