var express  = require('express');
var bodyparser = require('body-parser');
var Employeeroutes = require('../Employess/employe.routes');

var Teamsroutes = require('../Teams/teams.routes');

module.exports = function(){
    var app = express();

    app.use(bodyparser.json());
    app.use(bodyparser.urlencoded({extended:true}));

    Employeeroutes(app);
  
    Teamsroutes(app);

    return app;
}